import mongoose from "mongoose";

const Schema = mongoose.Schema;

let Todo = new Schema({
  title: {
    type: String,
  },
  status: {
    type: String,
    default: "Open",
  },
  color: {
    type: String,
    default: "#73b8bf",
  },
  position: {
    type: Number,
  },
});

export default mongoose.model("Todo", Todo);
