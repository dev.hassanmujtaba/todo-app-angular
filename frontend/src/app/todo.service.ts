import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class TodoService {
  uri = 'http://localhost:4000';
  constructor(private http: HttpClient) {}

  getTodos() {
    return this.http.get(`${this.uri}/todos`);
  }
  getTodoByID(id: String) {
    return this.http.get(`${this.uri}/todos/${id}`);
  }
  addTodo(title: String, position: Number) {
    const todo = {
      title: title,
      position: position,
    };
    return this.http.post(`${this.uri}/todos/add`, todo);
  }
  updateTodo(id: String, title: String, color: String, position: String, status: String) {
    const todo = {
      title: title,
      color: color,
      position: position,
      status: status
    };
    return this.http.post(`${this.uri}/todos/update/${id}`, todo);
  }
  deleteTodo(id: String) {
    return this.http.get(`${this.uri}/todos/delete/${id}`);
  }
}
