import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Todo } from '../../todo.model';
import { TodoService } from '../../todo.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css'],
})
export class TodosComponent implements OnInit {
  public updateHide: boolean = false;
  todos: Todo[] = [];
  constructor(private todoService: TodoService) {
  }
  ngOnInit(): void {
    this.fetchTodos();
  }
  fetchTodos() {
    this.todoService.getTodos().subscribe((data: any) => {
      this.todos = data;
      console.log(this.todos);
      for (let i = 0; i < this.todos.length; i++) {
        const title = this.todos[i].title;
        const color = this.todos[i].color;
        const status = this.todos[i].status;
        const position = this.todos[i].position;
        console.log(title);
        console.log(status);
        console.log(color);
        console.log(position);
      }
    });
  }
  deleteTodo(id: String) {
    this.todoService.deleteTodo(id).subscribe(() => {
      this.fetchTodos();
    });
  }
  addTodo(title: String, position: Number) {
    this.todoService.addTodo(title, position).subscribe(() => {
      this.fetchTodos();
    });
  }
  updateTodo(id: String, title: String, color: String, position: String, status: String) {
    this.todoService.updateTodo(id, title, color, position, status).subscribe(() => {
      this.updateHide = false;
      this.fetchTodos();
    });
  }
  onDblClick(todo: Todo) {
    todo.editable = true;    
  }
  drop(event: CdkDragDrop<any>) {
    moveItemInArray(this.todos, event.previousIndex, event.currentIndex);
  }
}

