import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  // public name = 'Hassan Mujtaba';
  // public siteURl = window.location.href;
  // public isDisable = false;
  // public textSuccess = "text-success"
  // public hasError = true;
  // public isSpecial = true;
  // public messageClass = {
  //   "text-success": !this.hasError,
  //   "text-danger": this.hasError,
  //   "text-special": this.isSpecial
  // }
  title = 'toto-app';

  ngOnInit() {}
  // greetUser(){
  //   return "Hy " + this.name
  // }
}
