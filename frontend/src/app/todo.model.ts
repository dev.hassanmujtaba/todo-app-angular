export interface Todo {
  _id: String;
  title: String;
  status: String;
  color: any;
  position: String;
  editable: boolean;
}
